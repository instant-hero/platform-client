FROM node:latest

WORKDIR /usr/app/src

COPY . .
RUN npm i -g @angular/cli
RUN npm i

ENTRYPOINT ["npm", "run", "start"]
