import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FirstAidComponent } from './first-aid/first-aid.component';
import {DisasterRouting} from '../disaster/disaster-routing.module';
import {InformativeRouting} from './informative-routing.module';



@NgModule({
  declarations: [FirstAidComponent],
  imports: [
    CommonModule,
    InformativeRouting
  ]
})
export class InformativeModule { }
