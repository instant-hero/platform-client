import { Routes, RouterModule } from '@angular/router';
import {FirstAidComponent} from './first-aid/first-aid.component';

const routes: Routes = [
  {path: 'first-aid', component: FirstAidComponent}
];

export const InformativeRouting = RouterModule.forRoot(routes);
