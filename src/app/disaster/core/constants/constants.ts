export class Constants {
  static Disaster = class {
    public static prefix = '/disasters';
    public static types = '/types';
    public static report = '/report';
    public static volunteer = '/volunteer';
    public static volunteers = '/volunteers';
    public static guest = '/guest';
    public static essentials = '/essentials';
    public static resolve = '/resolved';
    public static actions = '/actions';
    public static items = '/items';
  };
}
