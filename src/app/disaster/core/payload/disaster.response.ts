import {DisasterTypeResponse} from './disaster-type.response';
import {UserResponse} from '../../../user/core/payload/user.response';
import {MetaInfoResponse} from './meta-info.response';

export class DisasterResponse {
    constructor(
        public id: number = 0,
        public coordinates: string = '',
        public dateStarted: Date = new Date(),
        public type: MetaInfoResponse = null,
        public resolved: boolean = false,
        public description: string = '',
        public essentials: string = '',
        public finalMessage: string = '',
        public dateResolved: Date = new Date(),
        public volunteers: UserResponse[] = [],
        public outerHelp: number = 0,
        public kmDistance: string = ''
    ) {

    }
}
