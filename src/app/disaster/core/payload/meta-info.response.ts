export class MetaInfoResponse {
  constructor(
    public id: number = 0,
    public name: string = '',
    public iconUrl: string = '',
    public selected = false,
    public actionId = 0
  ) {

  }
}
