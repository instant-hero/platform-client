import {MetaInfoResponse} from './meta-info.response';
import {UserResponse} from '../../../user/core/payload/user.response';

export class ParticipationResponse {
  constructor(
    public id: number = 0,
    public action: MetaInfoResponse,
    public item: MetaInfoResponse,
    public user: UserResponse
  ) {
  }
}
