import {ActionRequest} from './action.request';

export class VolunteerRequest {
  constructor(
    public email: string = '',
    public actions: ActionRequest[] = []
  ) {

  }
}
