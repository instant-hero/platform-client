export class CurrentDisasterMapModel {
  constructor(
    public id: number = 0,
    public latitude: number = 0,
    public longitude: number = 0,
    public title: string = '',
    public iconUrl: string = ''
  ) {

  }
}
