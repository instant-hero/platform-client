import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {DisasterTypeResponse} from '../payload/disaster-type.response';
import {environment} from '../../../../environments/environment';
import {Constants} from '../constants/constants';
import {DisasterResponse} from '../payload/disaster.response';
import {DisasterRequest} from '../payload/disaster.request';
import {EssentialsRequest} from '../payload/essentials.request';
import {FinalMessageRequest} from '../payload/final-message.request';
import {MetaInfoResponse} from '../payload/meta-info.response';
import {VolunteerRequest} from '../payload/volunteer.request';
import {ParticipationResponse} from '../payload/participation.response';

export class DisasterService {
  constructor(
    private http: HttpClient
  ) {

  }

  public getAll(): Observable<DisasterResponse[]> {
    return this.http.get<DisasterResponse[]>(
      environment.api
      + Constants.Disaster.prefix
    );
  }

  public getOneById(id: number): Observable<DisasterResponse> {
    return this.http.get<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
    );
  }

  public getTypes(): Observable<DisasterTypeResponse[]> {
    return this.http.get<DisasterTypeResponse[]>(
      environment.api
      + Constants.Disaster.prefix
      + Constants.Disaster.types
    );
  }

  public getActions(id): Observable<MetaInfoResponse[]> {
    return this.http.get<MetaInfoResponse[]>(
      environment.api
      + Constants.Disaster.prefix
      + Constants.Disaster.types
      + `/${id}`
      + Constants.Disaster.actions
    );
  }

  public getItems(id: number): Observable<MetaInfoResponse[]> {
    return this.http.get<MetaInfoResponse[]>(
      environment.api
      + Constants.Disaster.prefix
      + Constants.Disaster.actions
      + `/${id}`
      + Constants.Disaster.items
    );
  }

  public report(model: DisasterRequest): Observable<DisasterResponse> {
    return this.http.post<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix,
      model
    );
  }

  public volunteer(id: number, model: VolunteerRequest): Observable<DisasterResponse> {
    return this.http.patch<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
      + Constants.Disaster.volunteer,
      model
    );
  }

  public guestVolunteer(id: number): Observable<DisasterResponse> {
    return this.http.patch<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
      + Constants.Disaster.guest
      + Constants.Disaster.volunteer,
      {}
    );
  }

  public volunteersByDisasterId(id: number): Observable<ParticipationResponse[]> {
    return this.http.get<ParticipationResponse[]>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
      + Constants.Disaster.volunteers
    );
  }

  public essentials(id: number, model: EssentialsRequest): Observable<DisasterResponse> {
    return this.http.patch<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
      + Constants.Disaster.essentials,
      model
    );
  }

  public resolve(id: number, model: FinalMessageRequest): Observable<DisasterResponse> {
    return this.http.patch<DisasterResponse>(
      environment.api
      + Constants.Disaster.prefix
      + `/${id}`
      + Constants.Disaster.resolve,
      model
    );
  }

}
