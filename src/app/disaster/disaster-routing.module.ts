import { Routes, RouterModule } from '@angular/router';
import {LandingComponent} from './components/landing/landing.component';
import {ReportComponent} from './components/report/report.component';
import {DisastersListComponent} from './components/disasters-list/disasters-list.component';
import {DisasterDetailsComponent} from './components/disaster-details/disaster-details.component';
import {ParticipationFormComponent} from './components/participation-form/participation-form.component';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: LandingComponent},
  {path: 'report', component: ReportComponent},
  {path: 'disasters', component: DisastersListComponent},
  {path: 'disasters/:id', component: DisasterDetailsComponent},
  {path: 'disasters/:disasterId/:typeId/volunteer', component: ParticipationFormComponent}
];

export const DisasterRouting = RouterModule.forRoot(routes);
