import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LandingGuestsComponent} from './components/landing-guests/landing-guests.component';
import {MapComponent} from './components/map/map.component';
import {AgmCoreModule} from '@agm/core';
import {DisasterRouting} from './disaster-routing.module';
import { LandingComponent } from './components/landing/landing.component';
import { ReportComponent } from './components/report/report.component';
import {DisasterService} from './core/service/disaster.service';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import { DisastersListComponent } from './components/disasters-list/disasters-list.component';
import { DisasterSingleComponent } from './components/disaster-single/disaster-single.component';
import { DisasterDetailsComponent } from './components/disaster-details/disaster-details.component';
import {SharedModule} from '../shared/shared.module';
import {environment} from "../../environments/environment";
import { MetaItemComponent } from './components/meta-item/meta-item.component';
import { ParticipationFormComponent } from './components/participation-form/participation-form.component';
import { ResolvedFormComponent } from './components/resolved-form/resolved-form.component';
import { EssentialsFormComponent } from './components/essentials-form/essentials-form.component';
import { StatsComponent } from './components/stats/stats.component';

@NgModule({
  declarations: [
    LandingGuestsComponent,
    MapComponent,
    LandingComponent,
    ReportComponent,
    DisastersListComponent,
    DisasterSingleComponent,
    DisasterDetailsComponent,
    MetaItemComponent,
    ParticipationFormComponent,
    ResolvedFormComponent,
    EssentialsFormComponent,
    StatsComponent
  ],
  providers: [
    DisasterService
  ],
  imports: [
    DisasterRouting,
    CommonModule,
    HttpClientModule,
    SharedModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleApiKey,
      libraries: ['places']
    }),
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule
  ]
})
export class DisasterModule {
}
