import {Component, OnInit} from '@angular/core';
import {DisasterResponse} from '../../core/payload/disaster.response';
import {DisasterRequest} from '../../core/payload/disaster.request';
import {DisasterService} from '../../core/service/disaster.service';
import {ToastrService} from 'ngx-toastr';
import {ActivatedRoute} from '@angular/router';
import {EssentialsRequest} from '../../core/payload/essentials.request';
import {FinalMessageRequest} from '../../core/payload/final-message.request';
import {AuthenticationService} from '../../../shared/service/authentication.service';
import {MetaInfoResponse} from '../../core/payload/meta-info.response';

@Component({
  selector: 'ih-disaster-details',
  templateUrl: './disaster-details.component.html',
  styleUrls: ['./disaster-details.component.scss']
})
export class DisasterDetailsComponent implements OnInit {
  public editingEssentials = false;
  public resolving = false;
  public participating = false;

  public hasVolunteered = false;

  public disaster: DisasterResponse;
  public finalMessageModel: FinalMessageRequest = new FinalMessageRequest();
  public actions: MetaInfoResponse[] = [];
  public actionItems: { [id: number]: MetaInfoResponse[] } = {};

  public actionUsers: { [name: string]: number[] } = {};
  public itemUsers: { [name: string]: number[] } = {};

  constructor(
    private disasterService: DisasterService,
    private toastrService: ToastrService,
    private route: ActivatedRoute,
    public authenticationService: AuthenticationService
  ) {
  }

  ngOnInit() {
    this.getDisaster();
  }

  getDisaster() {
    this.disasterService.getOneById(this.route.snapshot.params.id)
      .subscribe(res => {
        this.disaster = res;
        this.getDisasterActions(res.type.id);
        this.getDisasterVolunteers(res.id);
      });
  }

  getDisasterActions(id: number) {
    this.disasterService.getActions(id)
      .subscribe(res => {
        this.actions = res;

        res.forEach(a => {
          this.disasterService.getItems(a.id)
            .subscribe(items => {
              if (!this.actionItems[a.id]) {
                this.actionItems[a.id] = [];
              }

              this.actionItems[a.id] = items;
            });
        });
      });
  }

  getDisasterVolunteers(id: number) {
    this.disasterService.volunteersByDisasterId(id)
      .subscribe(res => {
        res.forEach(p => {
          if (p.action !== null) {
            if (!this.actionUsers.hasOwnProperty(p.action.name)) {
              this.actionUsers[p.action.name] = [];
            }

            if (this.actionUsers[p.action.name].indexOf(p.user.id) === -1) {
              this.actionUsers[p.action.name].push(p.user.id);

              if (p.user.id === +this.authenticationService.getId()) {
                this.hasVolunteered = true;
              }
            }
          }

          if (p.item !== null) {
            if (!this.itemUsers.hasOwnProperty(p.item.name)) {
              this.itemUsers[p.item.name] = [];
            }

            if (this.itemUsers[p.item.name].indexOf(p.user.id) === -1) {
              this.itemUsers[p.item.name].push(p.user.id);
            }
          }

        });
      });
  }

  saveEssentials(model: EssentialsRequest) {
    this.disasterService.essentials(this.route.snapshot.params.id, model)
      .subscribe(() => {
        this.toastrService.success('Успешно добавена информация.');
        this.disaster.essentials = model.essentials;
        this.editingEssentials = false;
      }, () => {
        this.toastrService.error('Грешка. Моля прверете валидността на данните си.');
      });
  }

  resolve(model: FinalMessageRequest) {
    this.disasterService.resolve(this.disaster.id, this.finalMessageModel)
      .subscribe(() => {
        this.toastrService.success('Бедствието е добавено към архива');
        this.getDisaster();
        this.resolving = false;
      }, () => {
        this.toastrService.error('Възникна грешка.');
      });
  }

  getObjectNames(object: { [p: string]: number[] }) {
    return Object.keys(object);
  }
}
