import {Component, OnInit} from '@angular/core';
import {DisasterService} from '../../core/service/disaster.service';
import {CurrentDisasterMapModel} from '../../core/payload/current-disaster-map.model';
import {Router} from '@angular/router';
import {DisasterResponse} from '../../core/payload/disaster.response';

@Component({
  selector: 'ih-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  public activeDisasters = false;
  public currentDisastersMap: CurrentDisasterMapModel[] = [];
  public currentDisasters: DisasterResponse[] = [];
  public pastDisasters: DisasterResponse[] = [];
  public hasChecked = false;
  public currCoords = '';

  constructor(
    private disasterService: DisasterService,
    private router: Router
  ) {
  }

  ngOnInit() {
    this.disasterService.getAll()
      .subscribe(res => {
        res.forEach(d => {
          if (!d.resolved) {
            this.activeDisasters = true;

            const [lat, lon] = d.coordinates.split(', ');

            this.currentDisasters.push(d);

            this.currentDisastersMap.push(
              new CurrentDisasterMapModel(
                d.id,
                +lat,
                +lon,
                d.type.name,
                'http://pngimg.com/uploads/attention/attention_PNG60.png'
              )
            );
          } else {
            this.pastDisasters.push(d);
          }
        });

        this.hasChecked = true;
      });
  }

  redirectToDisaster($event: number) {
    this.router.navigate(['disasters', $event]);
  }

  onLocationEmitted($event: string) {
    this.currCoords = $event;

    this.currentDisasters.forEach(d => {
      const [lat1, lon1] = d.coordinates.split(', ');
      const [lat2, lon2] = this.currCoords.split(', ');

      d.kmDistance = this.getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2).toFixed(1);

      console.log(d.kmDistance);
    });

    this.currentDisasters = this.currentDisasters.sort((a, b) => +a.kmDistance - +b.kmDistance);
  }

  getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    const R = 6371; // Radius of the earth in km
    const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
    const dLon = this.deg2rad(lon2 - lon1);
    const a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2);

    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI / 180);
  }
}
