import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FinalMessageRequest} from '../../core/payload/final-message.request';

@Component({
  selector: 'ih-resolved-form',
  templateUrl: './resolved-form.component.html',
  styleUrls: ['./resolved-form.component.scss']
})
export class ResolvedFormComponent implements OnInit {
  @Output()
  public finalMessageEmitter = new EventEmitter<FinalMessageRequest>();
  public model: FinalMessageRequest = new FinalMessageRequest();

  constructor() {
  }

  ngOnInit() {
  }

  resolve() {
    this.finalMessageEmitter.emit(this.model);
  }
}
