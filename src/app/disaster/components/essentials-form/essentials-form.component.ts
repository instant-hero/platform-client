import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FinalMessageRequest} from '../../core/payload/final-message.request';
import {EssentialsRequest} from '../../core/payload/essentials.request';

@Component({
  selector: 'ih-essentials-form',
  templateUrl: './essentials-form.component.html',
  styleUrls: ['./essentials-form.component.scss']
})
export class EssentialsFormComponent implements OnInit {
  @Output()
  public essentialsEmitter = new EventEmitter<EssentialsRequest>();

  @Input()
  public currentEssentials = '';

  public model: EssentialsRequest = new EssentialsRequest();

  constructor() {
  }

  ngOnInit() {
    this.model.essentials = this.currentEssentials;
  }

  saveEssentials() {
    console.log(this.model);
    this.essentialsEmitter.emit(this.model);
  }
}
