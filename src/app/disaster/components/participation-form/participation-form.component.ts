import {Component, OnInit} from '@angular/core';
import {MetaInfoResponse} from '../../core/payload/meta-info.response';
import {ActivatedRoute, Router} from '@angular/router';
import {DisasterService} from '../../core/service/disaster.service';
import {VolunteerRequest} from '../../core/payload/volunteer.request';
import {ActionRequest} from '../../core/payload/action.request';
import {AuthenticationService} from '../../../shared/service/authentication.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'ih-participation-form',
  templateUrl: './participation-form.component.html',
  styleUrls: ['./participation-form.component.scss']
})
export class ParticipationFormComponent implements OnInit {
  public currentStep = 1;

  public disasterId = 0;
  public typeId = 0;

  public disasterActions: MetaInfoResponse[];
  public actionsItems: MetaInfoResponse[] = [];
  // public actionsItems: { [actionId: number]: MetaInfoResponse[] } = {};

  public model: VolunteerRequest = new VolunteerRequest();

  public actionIds: number[] = [];
  public itemIds: number[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private disasterService: DisasterService,
    private authenticationService: AuthenticationService,
    public toastrService: ToastrService
  ) {
  }

  ngOnInit() {
    this.disasterId = this.route.snapshot.params.disasterId;
    this.typeId = this.route.snapshot.params.typeId;

    this.route.queryParams.subscribe(params => {
      this.currentStep = params.currentStep ? JSON.parse(params.currentStep) : 1;
    });

    if (this.currentStep === 1) {
      this.getActions();
    }

    if (this.currentStep === 2) {
      this.getItems();
    }

    if (this.currentStep === 3) {
      if (!this.authenticationService.isAuthenticated()) {
        this.processModel();
      } else {
        this.currentStep = 4;
      }
    }

    if (this.currentStep === 4) {

    }
  }

  public getActions(): void {
    this.disasterService.getActions(this.typeId)
      .subscribe(res => {
        this.disasterActions = res;

        this.route.queryParams.subscribe(params => {
          this.actionIds = params.actions ? JSON.parse(params.actions) : [];
          this.itemIds = params.items ? JSON.parse(params.items) : [];

          this.disasterActions.forEach(action => {
            if (this.actionIds.indexOf(action.id) !== -1) {
              action.selected = true;
            }
          });

        });
      });
  }

  public getItems(): void {
    this.route.queryParams.subscribe(params => {
      this.actionIds = params.actions ? JSON.parse(params.actions) : [];
      this.itemIds = params.items ? JSON.parse(params.items) : [];

      this.actionIds.forEach(a => {
        this.disasterService.getItems(a)
          .subscribe(res => {
            res.forEach(r => {
              r.actionId = a;
              if (this.itemIds.indexOf(r.id) !== -1) {
                r.selected = true;
              }

              if (this.actionsItems.map(ai => ai.id).indexOf(r.id) === -1) {
                this.actionsItems.push(r);
              }
            });
          });
      });

      this.actionsItems.forEach(a => {
        if (!this.model.actions.find(ar => ar.id === a.actionId)) {
          this.model.actions.push(new ActionRequest(a.id, []));
        }

        if (this.model.actions.find(ar => ar.id === a.actionId).items.indexOf(a.id) === -1 && a.selected) {
          this.model.actions.find(ar => ar.id === a.actionId).items.push(a.id);
        }

      });
    });
  }

  selectAction(action: MetaInfoResponse) {
    if (action.selected) {
      this.actionIds = this.actionIds.filter(id => id !== action.id);
      action.selected = false;
    } else {
      this.actionIds.push(action.id);
      action.selected = true;
    }

    this.navigate();
  }

  selectItem(item: MetaInfoResponse) {
    if (item.selected) {
      this.itemIds = this.itemIds.filter(id => id !== item.id);
      item.selected = false;
    } else {
      this.itemIds.push(item.id);
      item.selected = true;
    }
  }

  public navigate() {
    this.router.navigate(['disasters', this.disasterId, this.typeId, 'volunteer'], {
      queryParams: {
        actions: JSON.stringify(this.actionIds),
        items: JSON.stringify(this.itemIds),
        currentStep: JSON.stringify(this.currentStep)
      }
    });
  }

  public incrementStep() {
    this.currentStep++;
    this.navigate();

    if (this.currentStep === 2) {
      this.getItems();
    }

    if (this.currentStep === 3) {
      if (this.authenticationService.isAuthenticated()) {
        this.processModel();
      } else {
        this.incrementStep();
        this.currentStep = 4;
      }
    }

    if (this.currentStep === 5) {
      this.toastrService
        .success('Благодарим! На зададения имейл изпратихме парола за вход в системата.');
      this.router.navigate(['disasters', this.disasterId]);
    }
  }

  public processModel() {
    this.getItems();

    // if (this.model.actions.map(ar => ar.id))
    this.actionIds.forEach(da => {
      if (this.model.actions.map(ar => ar.id).indexOf(da) === -1) {
        this.model.actions.push(new ActionRequest(da, []));
      }
    });

    console.log(this.model);

    if (this.authenticationService.isAuthenticated()) {
      this.disasterService.volunteer(this.disasterId, this.model)
        .subscribe(() => {
          this.toastrService.success('Благодарим ти! Успешно заяви помощта си');
          this.router.navigate(['disasters', this.disasterId]);
        }, () => {
          this.toastrService.error('Възникна грешка.');
          this.router.navigate(['disasters', this.disasterId]);
        });
    }
  }
}
