import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthenticationService} from './service/authentication.service';
import { NavbarComponent } from './navbar/navbar.component';
import {RouterModule} from '@angular/router';
import { TopComponent } from './top/top.component';

@NgModule({
  declarations: [NavbarComponent, TopComponent],
  providers: [
    AuthenticationService
  ],
  exports: [
    NavbarComponent,
    TopComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ]
})
export class SharedModule { }
