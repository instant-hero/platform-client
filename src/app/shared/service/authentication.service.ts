import {HttpClient} from '@angular/common/http';
import {EmailRequest} from '../payload/email.request';
import {environment} from '../../../environments/environment';

export class AuthenticationService {
  constructor(
    private http: HttpClient
  ) {

  }

  public saveUserData(token: string, userId: number) {
    localStorage.setItem('token', token);
    localStorage.setItem('userId', userId + '');
  }

  public isAuthenticated() {
    return localStorage.getItem('token');
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  public getId() {
    return localStorage.getItem('userId');
  }

  public setOuterHelped(disasterId: number) {
    localStorage.setItem(disasterId + '', 'true');
  }

  public getHelpingData(disasterId: number) {
    return localStorage.getItem(disasterId + '') === 'true';
  }

  public logout() {
    localStorage.clear();
  }

  public checkEmail(email: string) {
    return this.http.get(
      environment.api
      + '/auth'
      + '/' + email
    );
  }
}
