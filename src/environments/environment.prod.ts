export const environment = {
  production: true,
  api: '/api',
  googleApiKey: '${GMAPS_API_KEY}'
};
